const mocha = require('mocha');
const assert = require('assert');
const utility = require('../src/utility/utility.js');
const yaml = require('js-yaml');
const fs = require('fs');

var inputData = yaml.load(fs.readFileSync('./test/data.js'));

describe('test utility functions', () => {
    describe('transformCarlist', () => {
        it('returns the json data grouped based on car make', () => {
            const returnData = utility.transformCarlist(inputData);
            const expectedOutput = yaml.load(fs.readFileSync('./test/expectedResponseData.json'))
            assert.equal(JSON.stringify(returnData), JSON.stringify(expectedOutput));    
        });
    });
});