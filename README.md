# CarList Service

This service calls a backend api (which provides a list of cars)as described in problem description. It transforms the data from backend api to the a format in the problem statement.
The link for problem statement is as below :
http://eacodingtest.digital.energyaustralia.com.au/

##Contributors:

  Owner: Deepika Handa

  contributors : Deepika Handa


##Installation

Firts make sure you have `node` and `npm` installed.

Then clone the git repo from the below URL:


cd to the project Folder

Install dependencies listed in package.json
   
   $npm install


#Running the server

Run the server on your local Machine
   
   $npm start

#Consuming the Service
Use any http Client like `postman` to hit the service hosted on:

http://localhost:5000/api/v1/cars


#Project Structure
├── src                        # source files
│   ├── utility
│   │   ├── utility.js         # utility file to do all the transformation operation
│   ├── carListController.js   # controller class which provide the end point for the rest service
│   ├── carListService         # This Service file has code that hits the backend APIs and gets the data
├── test
|   ├── utility.test.js        # test file which tests the code in utility.js
├── package.json               # contains all the dependancies, npm run scripts
├── .gitignore                 # contains all the files to be ignored while doing git commit.
├── .eslintrc.js               # encompases all the liniting rules                   
└── README.md                  # contains all the description related to project

##test
   $npm test

