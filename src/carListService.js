const constants = require('../constants.json');
const http = require('http');
const utility = require('./utility/utility.js');

function getData(url,callback) {
   const req = http.get(url, res => {
        let body = '';
        let error = '';
        res.setEncoding('utf8');

        res.on('data', data => {
            body += data;
        });
        res.on('end', () => {
            if (body !== '""' && body !== constants.SERVER_ERROR_RES) {
                 body = utility.transformCarlist(JSON.parse(body)); //call utility function
            } else {
                error = constants.SERVER_ERROR;
            }
            callback(error, body); //call callback function after data is received from backend service
        });
    });
req.end();
}

module.exports.getData = getData;