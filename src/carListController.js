const express = require('express');
const constants = require('../constants.json');
const carListService = require('./carListService.js');
const signale = require('signale');

// Set up the express app
const app = express();

// get all cars
app.get('/api/v1/cars', (req, response) => {
    carListService.getData(constants.URL, function (err, carList) {
        if (err) {
            response.status(500).send({
                error: err,
            });
        } else {
            response.status(200).send({
                carList,
            });
        }
    });
});

app.listen(constants.PORT, () => {
    signale.success(`server running on port ${constants.PORT}`);
});
