const _ = require('underscore');
const constants = require('../../constants.json');

const transformCarlist = (data) => {
    const carlist = [];
    //assign carshow to each car
    data.forEach(carShow => carShow.cars.map(car => {
        car.carshow = carShow.name;
        carlist.push(car);
    }));
    const sortedCarlist = _.sortBy(carlist, constants.MAKE); //sort car list by make
    const groupedbyMake = _.groupBy(sortedCarlist, constants.MAKE);// group all the cars by make
    const makekeys = (Object.keys(groupedbyMake)); //fetch make as keys
    makekeys.forEach(makekey => { //iterate through each make
        const groupByModel = _.groupBy(groupedbyMake[makekey], constants.MODEL); //group cars by Model for each make
        Object.keys(groupByModel).forEach(modelkey => { //iterate through each model as key
            const pluckedCarshow = _.uniq(_.pluck(groupByModel[modelkey], constants.CARSHOW)); // fetch only carshow value from a car
            groupByModel[modelkey] = pluckedCarshow; //for each model replace each model value (car object) with only carshow name
        });
        groupedbyMake[makekey] = groupByModel; //for each make replace value (car object) with newly formed model object
    });
    return (groupedbyMake);
};

module.exports = {
    transformCarlist,
};